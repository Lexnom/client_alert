from PyQt5 import QtCore, QtGui, QtWidgets, Qt, sip
import socket, json, sys
import config
import re

from PyQt5.QtWidgets import QMessageBox

class PopupWindowClass(QtWidgets.QWidget):
    popuphidden = QtCore.pyqtSignal()

    def __init__(self, text='Тестовое сообщение'):
        super(PopupWindowClass, self).__init__()
        self.setWindowFlags(QtCore.Qt.SplashScreen | QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

        self.setMinimumSize(QtCore.QSize(300, 100))
        self.animation = QtCore.QPropertyAnimation(self, b"windowOpacity", self)
        self.animation.finished.connect(self.hide)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.hideAnimation)
        self.setupUi()
        self.setPopupText(text)

    def setupUi(self):
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.label = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignLeft)
        self.verticalLayout.addWidget(self.label)
        appearance = self.palette()
        appearance.setColor(QtGui.QPalette.Normal, QtGui.QPalette.Window,
                     QtGui.QColor("silver"))
        self.setPalette(appearance)
    def setPopupText(self, text):
        self.label.setText(text)
        self.label.adjustSize()
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.dragPosition = event.globalPos() - self.frameGeometry().topLeft()
            event.accept()
    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.dragPosition)
            event.accept()
    def show(self):
        self.setWindowOpacity(0.0)
        self.animation.setDuration(1500)
        self.animation.setStartValue(0.0)
        self.animation.setEndValue(1.0)
        QtWidgets.QWidget.show(self)
        self.animation.start()
        self.timer.start(8000)

    def hideAnimation(self):
        self.timer.stop()
        self.animation.setDuration(1500)
        self.animation.setStartValue(1.0)
        self.animation.setEndValue(0.0)
        self.animation.start()
    def hide(self):
        if self.windowOpacity() == 0:
            QtWidgets.QWidget.close(self)
            self.popuphidden.emit()


def move2RightBottomCorner(win):
    try:
        screen_geometry = QtWidgets.QApplication.desktop().availableGeometry()
        screen_size = (screen_geometry.width(), screen_geometry.height())
        win_size = (win.frameSize().width(), win.frameSize().height())
        x = screen_size[0] - win_size[0] - 10
        y = screen_size[1] - win_size[1] - 10
        win.move(x, y)
    except Exception as e:
        print (e)

class WorkThread(Qt.QThread):
    threadSignalAlert = Qt.pyqtSignal(str)
    threadSignalAuth = Qt.pyqtSignal(bool, object)
    threadSignalError = Qt.pyqtSignal(str)
    threadSignaClose_socket = Qt.pyqtSignal(bool)
    def __init__(self, sock):
        super(WorkThread, self).__init__()
        self.sock = sock

    def run(self, *args, **kwargs):
        try:
            while True:
                response = self.sock.recv(1024).decode()
                response = json.loads(response)
                print(response['status'])

                if response['status'] == 'auth':
                    if response['status_auth'] == 'success':
                        self.threadSignalAuth.emit(True, self.sock)
                    else:
                        try:
                            self.threadSignalAuth.emit(False, self.sock)
                        except:
                            self.threadSignaClose_socket.emit(True)
                if response['status'] == 'alert':
                    if response['type'] == 'message':
                        text_message = response['text_alert']
                        if len(text_message) > 30:
                            text_message = text_message[0:30]+'...'
                        message = 'Новое сообщение от %s\n%s \nДата:%s' % (response['avtor'], text_message, response['date'])
                        self.threadSignalAlert.emit(message)
        except IOError:
            self.threadSignaClose_socket.emit(False)


class Ui_MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.setupUi(self)



    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(247, 140)
        MainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.loginButton = QtWidgets.QPushButton(self.centralwidget)
        self.loginButton.setGeometry(QtCore.QRect(80, 90, 91, 23))
        self.loginButton.setObjectName("loginButton")

        self.loginButton.clicked.connect(self.set_auth)

        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(80, 30, 113, 20))
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(80, 60, 113, 20))
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(40, 30, 41, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 60, 41, 16))
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def set_auth(self):
        login = self.lineEdit.text()
        password = self.lineEdit_2.text()
        if login != '' and password != '':
            try:
                self.connect_server(login, password)
            except Exception:
                auth_window.show()
        else:
            QMessageBox.warning(self, 'Предупреждение', 'Поля не заполнены', QMessageBox.Ok)

    def hide_main(self, check, sock):
        if check:
            self.hide()
        else:
            self.view_error_mes(('Ошибка авторизации', 'не верный логин или пароль'))
            sock.close()


    def view_error_mes(self, text):
        QMessageBox.warning(self, text[0], text[1], QMessageBox.Ok)

    def view_main(self, check):
        if not check:
            self.view_error_mes(('Ошибка соеденения с сервером', 'Потеряно соединение с сервером'))
        else:
            auth_window.show()

    def view_windows(self, text='test'):
        alert_window = PopupWindowClass(text)
        alert_window.show()
        move2RightBottomCorner(alert_window)

    def read_socket(self, sock):
            self.thrd = WorkThread(sock=sock)
            self.thrd.threadSignalAlert.connect(self.view_windows)
            self.thrd.threadSignalAuth.connect(self.hide_main)
            self.thrd.threadSignalError.connect(self.view_error_mes)
            self.thrd.threadSignaClose_socket.connect(self.view_main)
            self.thrd.start()




    def connect_server(self, login, password):
        ip, port = self.set_ip_port()
        print(ip, port)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            sock.connect((ip, port))
        except IOError:
            self.view_error_mes(('Ошибка соеденения с сервером', 'Не удалось установить соеденение с сервером'))

        data = {'status': 'auth', 'login': login, 'pass': password}
        data = json.dumps(data)
        sock.send(data.encode())
        self.read_socket(sock)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Авторизация"))
        self.loginButton.setText(_translate("MainWindow", "Войти"))
        self.label.setText(_translate("MainWindow", "Логин:"))
        self.label_2.setText(_translate("MainWindow", "Пароль:"))

    def set_ip_port(self):
        port = None
        ip = None
        data = open('client.conf', 'r')
        data_f = data.read()
        list_data_f = data_f.split('\n')
        for list in list_data_f:
            f = list.split('=')
            if f[0] == 'ip':
                ip = f[1]
            if f[0] == 'port':
                port = f[1]
        data.close()
        return ip, int(port)
if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    auth_window = Ui_MainWindow()
    auth_window.show()
    sys.exit(app.exec_())